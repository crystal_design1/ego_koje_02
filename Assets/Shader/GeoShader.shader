﻿Shader "Shader/GeoShader" {
	Properties {
		_EdgeLength("Edge length", Range(2,50)) = 15
		_Phong("Phong Strengh", Range(0,1)) = 0.5
		_AlbedoTex ("Albedo (RGB)", 2D) = "gray" {}
		_BumpMap("Bumpmap", 2D) = "bump" {}
		_Normal_Int("Normal_Int", Range(0 , 10)) = 2
		_Smoothness_Tex("Smoothness_Tex", 2D) = "black" {}
		_Smoothness_Int("Smoothness_Int", Range(0 , 1)) = 1
		_DispTex ("Disp Texture", 2D) = "gray" {}
		_DisplacementStrength("Displacement", Range(0, 1.0)) = 0.02
		_AO("Ambient Occlusion", 2D) = "white" {}
		_AO_Int("AO_Int", Range(0 , 2)) = 1
		//_AOContr("AO-Contr", Range(0 , 1)) = 0
		_WrinklesNormal("Wrinkles-Normal", 2D) = "bump" {}
		_Wrinkles_Int("Wrinkle_Normal_Int", Range(0 , 10)) = 4
		[HideInInspector] _texcoord3("", 2D) = "white" {}
		[HideInInspector] _texcoord("", 2D) = "white" {}
		[HideInInspector] __dirty("", Int) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		Cull Back
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		
		#pragma target 4.6
//#pragma surface surf Standard keepalpha fullforwardshadows vertex:disp tessellate:tessEdge tessphong:_Phong nolightmap
		#pragma surface surf Standard keepalpha fullforwardshadows vertex:disp tessellate:tessEdge tessphong:_Phong
		#include "UnityStandardUtils.cginc"
		#include "Tessellation.cginc"
		
		struct appdata {
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float2 texcoord : TEXCOORD0;
			float2 texcoord3 : TEXCOORD3;
		};

		uniform sampler2D _AlbedoTex;
		uniform float4 _AlbedoTex_ST;
		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;	//_ST is used for tiling
		uniform float _Normal_Int;
		uniform sampler2D _Smoothness_Tex;
		uniform float4 _Smoothness_Tex_ST;
		uniform float _Smoothness_Int;
		uniform sampler2D _DispTex;
		uniform sampler2D _AO;
		uniform float _DisplacementStrength;
		uniform float _EdgeLength;
		uniform float _Phong;
		uniform float _AO_Int;
		uniform float4 _AO_ST;
		//uniform float _AOContr;
		uniform sampler2D _WrinklesNormal;
		uniform float4 _WrinklesNormal_ST;
		uniform float _Wrinkles_Int;
		
		float4 tessEdge(appdata v0, appdata v1, appdata v2)
		{
			return UnityEdgeLengthBasedTess(v0.vertex, v1.vertex, v2.vertex, _EdgeLength);
		}

		void disp(inout appdata v)
		{
			float d = (tex2Dlod(_DispTex, float4(v.texcoord3.xy, 0, 0)).r);
			d = d - 0.5;
			float r = 0.03 / 0.5 * d;
			v.vertex.xyz += v.normal * r* _DisplacementStrength;
		}

		struct Input {
			float2 uv4_texcoord3;
			float2 uv_texcoord;
		};

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		float4 CalculateContrast(float contrastValue, float4 colorTarget)
		{
			//float t = 0.5 * (1.0 - contrastValue);
			float t = 0.5 * (contrastValue);
			return mul(float4x4(contrastValue, 0, 0, t, 0, contrastValue, 0, t, 0, 0, contrastValue, t, 0, 0, 0, 1), colorTarget);
		}
		
		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			float2 uv_Albedo_Tex = IN.uv_texcoord * _AlbedoTex_ST.xy + _AlbedoTex_ST.zw;
			o.Albedo = tex2D(_AlbedoTex, uv_Albedo_Tex).rgb;
			float2 uv4_WrinklesNormal = IN.uv4_texcoord3 * _WrinklesNormal_ST.xy + _WrinklesNormal_ST.zw;
			float2 uv_Normal_Tex = IN.uv_texcoord * _BumpMap_ST.xy + _BumpMap_ST.zw;
			o.Normal = BlendNormals(UnpackScaleNormal(tex2D(_WrinklesNormal, uv4_WrinklesNormal), _Wrinkles_Int), UnpackScaleNormal(tex2D(_BumpMap, uv_Normal_Tex), _Normal_Int));
			

			//Set smoothness
			float2 uv_Smoothness_Tex = IN.uv_texcoord * _Smoothness_Tex_ST.xy + _Smoothness_Tex_ST.zw;
			o.Smoothness = (tex2D(_Smoothness_Tex, uv_Smoothness_Tex) * _Smoothness_Int).r;

			// Compute Ambient Occlusion
			float2 uv4_AO = IN.uv4_texcoord3 * _AO_ST.xy + _AO_ST.zw;
			o.Occlusion = (tex2D(_AO, uv4_AO) * _AO_Int).r;
			//o.Occlusion = (CalculateContrast(_AOContr, colorTarget) * _AO_Int).r;
			o.Alpha = 1;
			//o.Smoothness = 0;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
